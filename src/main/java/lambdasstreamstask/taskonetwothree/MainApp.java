package lambdasstreamstask.taskonetwothree;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class MainApp {
    private static Logger LOG = LogManager.getLogger(MainApp.class);

    public static void main(String[] args) {
        LOG.info("Task 1+2 running:");
        Invoker.TaskOneAndTwoForLambdas();
        LOG.info("Task 3 running:");
        Invoker.TaskThreeForLambdas();
    }
}
