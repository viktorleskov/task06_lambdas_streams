package lambdasstreamstask.taskonetwothree;

import lambdasstreamstask.interfaces.ThreeIntegerator;
import lambdasstreamstask.utils.ListGenerator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.stream.Stream;

public abstract class Invoker {
    private static Logger LOG = LogManager.getLogger(Invoker.class);

    public static void TaskOneAndTwoForLambdas() {
        ThreeIntegerator maxOfThree = (a, b, c) -> (int) Stream.of(a, b, c).sorted().skip(2).toArray()[0];
        ThreeIntegerator averageOfThree = (a, b, c) -> (a + b + c) / 3;
        LOG.info(maxOfThree.calculate(9, 15, 3));
        LOG.info(maxOfThree.calculate(9, 12, 43));
        LOG.info(maxOfThree.calculate(43, 24, 3));
        LOG.info(averageOfThree.calculate(1, 4, 5));
        LOG.info(averageOfThree.calculate(9, 6, 5));
        LOG.info(averageOfThree.calculate(8, 7, 6));
    }

    public static void TaskThreeForLambdas() {
        int GENERATED_LIST_SIZE = 10;
        int MAX_LIST_VALUE = 50;
        List<Integer> testList = ListGenerator.generateRandomIntegerList(GENERATED_LIST_SIZE, MAX_LIST_VALUE);
        System.out.println("List of integers: ");
        testList.forEach((integer) -> System.out.print(integer + " "));
        printVoidLine();
        //sum via Optional
        testList
                .stream()
                .reduce(Integer::sum)
                .ifPresent(val -> LOG.info("Average val: " + val / testList.size()));
        testList
                .stream()
                .reduce(Integer::sum)
                .ifPresent(val -> LOG.info("Sum of list Elements: " + val));
        //main value via stream
        LOG.info("Min value: " + testList
                .stream()
                .sorted()
                .toArray()[0]);
        //via optional
        testList
                .stream()
                .reduce((par1, par2) -> par1 < par2 ? par1 : par2)
                .ifPresent(LOG::info);
        //max value via stream
        LOG.info("Max value: " + testList
                .stream()
                .sorted()
                .toArray()[testList.size() - 1]);
        //via optional
        testList
                .stream()
                .reduce((par1, par2) -> par1 > par2 ? par1 : par2)
                .ifPresent(LOG::info);
        //Count number of values bigger than average
        LOG.info("Count of values bigger than average " + testList
                .stream()
                .filter(e -> e > testList
                        .stream()
                        .reduce(Integer::sum)
                        .get() / testList.size())
                .count());
    }

    private static void printVoidLine() {
        System.out.println("\n");
    }
}
