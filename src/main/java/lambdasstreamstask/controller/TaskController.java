package lambdasstreamstask.controller;

import lambdasstreamstask.model.ModelDataSet;
import lambdasstreamstask.view.ConsoleView;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Optional;

public abstract class TaskController {
    private static TaskController singleton;
    private static Logger LOG = LogManager.getLogger(TaskController.class);

    private TaskController() {
    }

    public static TaskController ask() {
        if (Optional.ofNullable(singleton).isPresent()) return singleton;
        singleton = new TaskController() {
        };
        return ask();
    }

    public TaskController inputText() {
        LOG.info("Asking ModelDataSet for task(inputTextFromStdin)");
        ModelDataSet.ask().inputTextFromStdin();
        return this;
    }

    public TaskController calculateNumberOfUniqueWords() {
        LOG.info("Asking ModelDataSet for task(calculateNumberOfUniqueWords)");
        ConsoleView.printToConsole("Unique words value: " + ModelDataSet.ask().calculateNumberOfUniqueWords());
        return this;
    }

    public TaskController getSortedListOfWords() {
        LOG.info("Asking ModelDataSet for task(getSortedListOfWords)");
        ConsoleView.printToConsole("Sorted words: " + ModelDataSet.ask().getSortedListOfWords());
        return this;
    }

    public TaskController getWordCountByGrouping() {
        LOG.info("Asking ModelDataSet for task(getWordCountByGrouping)");
        ConsoleView.printToConsole("Words by Grouping:" + ModelDataSet.ask().getWordCountByGrouping());
        return this;
    }

    public TaskController getSymbolWithUpperCase() {
        LOG.info("Asking ModelDataSet for task(getSymbolWithLowerCase)");
        ConsoleView.printToConsole("Symbols with Upper case" + ModelDataSet.ask().getSymbolWithLowerCase());
        return this;
    }

}
