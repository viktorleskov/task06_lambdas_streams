package lambdasstreamstask.model;

import lambdasstreamstask.model.utils.SentenceUtils;

import java.util.List;

public class Sentence {
    private List<Word> words;

    public Sentence(String sentence) {
        this.words = SentenceUtils.parseWords(sentence, SentenceUtils.DEFAULT_REGEX_FOR_SENTENCE);
    }

    @Override
    public String toString() {
        StringBuilder bufString = new StringBuilder();
        for (Word word : words) {
            bufString.append(word.toString()).append(" ");
        }
        return bufString.toString();
    }

    public List<Word> getWords() {
        return words;
    }
}
