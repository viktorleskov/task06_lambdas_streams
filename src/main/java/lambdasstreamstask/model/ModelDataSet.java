package lambdasstreamstask.model;

import lambdasstreamstask.model.utils.TextUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;

public abstract class ModelDataSet {
    private static ModelDataSet singleton;
    private static Logger LOG = LogManager.getLogger(ModelDataSet.class);
    private static Text text;

    public static ModelDataSet ask() {
        if (Optional.ofNullable(singleton).isPresent()) return singleton;
        singleton = new ModelDataSet() {
        };
        return ask();
    }

    private ModelDataSet() {
    }

    private static List<Word> asWordList() {
        List<Word> list = new LinkedList<>();
        text
                .getSentences()
                .forEach(sentence -> list.addAll(sentence.getWords()));
        return list;
    }

    private static List<Character> asCharacterList() {
        List<Character> symbols = new LinkedList<>();
        asWordList().forEach(word -> symbols.addAll(word.getSymbols()));
        return symbols;
    }

    private static int countWordInText(Word wrd) {
        List<Word> words = asWordList();
        AtomicReference<Integer> count = new AtomicReference<>(0);
        words.forEach((Word word) -> count.set(count.get() + (word.equals(wrd) ? 1 : 0)));
        return count.get();
    }

    private static int countSymbolInText(Character charac) {
        List<Character> characters = asCharacterList();
        AtomicReference<Integer> count = new AtomicReference<>(0);
        characters.forEach((Character character) -> count.set(count.get() + (character.equals(charac) ? 1 : 0)));
        return count.get();
    }

    public String getSymbolWithLowerCase() {
        LOG.info("Task (getSymbolWithLowerCase) running!");
        return asCharacterList()
                .stream()
                .filter(Character::isLowerCase)
                .collect(groupingBy(ModelDataSet::countSymbolInText))
                .toString();
    }

    public void inputTextFromStdin() {
        LOG.info("Asking TextUtils for task(inputTextFromStdin)");
        text = new Text(TextUtils.inputTextFromStdin());
    }

    public long calculateNumberOfUniqueWords() {
        LOG.info("Task (calculateNumberOfUniqueWords) running!");
        List<Word> words = asWordList();
        return words
                .stream()
                .distinct()
                .count();
    }

    public String getSortedListOfWords() {
        LOG.info("Task (getSortedListOfWords) running!");
        List<Word> words = asWordList();
        return words
                .stream()
                .distinct()
                .sorted(Comparator.comparing(Word::hashCode))
                .collect(Collectors.toList())
                .toString();
    }

    public String getWordCountByGrouping() {
        LOG.info("Task (getWordCountByGrouping) running!");
        List<Word> words = asWordList().stream().distinct().collect(Collectors.toList());
        return words
                .stream()
                .collect(groupingBy(ModelDataSet::countWordInText))
                .toString();
    }
}
