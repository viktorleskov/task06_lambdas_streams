package lambdasstreamstask.model;

import java.util.ArrayList;
import java.util.List;

public class Word {
    private List<Character> symbols;

    public Word(String word) {
        this.symbols = new ArrayList<>();
        for (int i = 0; i < word.length(); i++) {
            this.symbols.add(word.charAt(i));
        }
    }

    public List<Character> getSymbols() {
        return symbols;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        symbols.forEach(stringBuilder::append);
        return stringBuilder.toString();
    }

    @Override
    public int hashCode() {
        return symbols.size() + symbols.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (o.getClass() != Word.class) return false;
        return this.hashCode() == o.hashCode();
    }
}
