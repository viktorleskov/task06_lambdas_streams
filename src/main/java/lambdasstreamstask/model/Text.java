package lambdasstreamstask.model;

import lambdasstreamstask.model.utils.TextUtils;

import java.util.List;

public class Text {
    private List<Sentence> sentences;

    public Text(String fileName) {
        sentences = TextUtils.parseSentence(fileName, TextUtils.DEFAULT_REGEX_FOR_FILE);
    }

    public List<Sentence> getSentences() {
        return sentences;
    }

    @Override
    public String toString() {
        StringBuilder bufString = new StringBuilder();
        for (Sentence sens : sentences) {
            bufString.append(sens).append("\n");
        }
        return bufString.toString();
    }
}
