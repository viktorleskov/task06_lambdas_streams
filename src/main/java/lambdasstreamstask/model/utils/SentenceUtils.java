package lambdasstreamstask.model.utils;

import lambdasstreamstask.model.Word;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class SentenceUtils {
    public static final String DEFAULT_REGEX_FOR_SENTENCE = "[\\w’`+]+";
    private static Pattern pattern;
    private static Matcher matcher;

    private SentenceUtils() {
    }

    public static List<Word> parseWords(String sentence, String regex) {
        List<Word> words = new ArrayList<>();
        pattern = Pattern.compile(regex);
        matcher = pattern.matcher(sentence);
        while (matcher.find()) {
            words.add(new Word(sentence.substring(matcher.start(), matcher.end())));
        }
        return words;
    }

}
