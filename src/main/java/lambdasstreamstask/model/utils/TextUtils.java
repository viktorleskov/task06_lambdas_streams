package lambdasstreamstask.model.utils;

import lambdasstreamstask.model.Sentence;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public final class TextUtils {
    public static final String DEFAULT_FILE_PATH = "system.out";
    public static final String DEFAULT_REGEX_FOR_FILE = "[^.]+";
    private static Logger LOG = LogManager.getLogger(TextUtils.class);
    private static PrintStream DEFAULT_STDOUT = System.out;
    private static Pattern pattern;
    private static Matcher matcher;

    private TextUtils() {
    }

    public static List<Sentence> parseSentence(String fileName, String regex) {
        List<Sentence> sentences = new ArrayList<>();
        String stringFile = fileToString(fileName);
        pattern = Pattern.compile(regex);
        matcher = pattern.matcher(stringFile);
        while (matcher.find()) {
            sentences.add(new Sentence(stringFile.substring(matcher.start(), matcher.end())));
        }
        return sentences;
    }

    private static String fileToString(String fileName) {
        StringBuilder stringText = new StringBuilder();
        try (BufferedReader br = Files.newBufferedReader(Paths.get(fileName))) {
            stringText.append(br.lines().collect(Collectors.toList()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return stringText.toString();
    }

    public static String inputTextFromStdin() {
        stdOutToFile();
        Scanner scan = new Scanner(System.in);
        String buffer;
        LOG.info("Please input your text. Empty line if end.");
        while (!Objects.equals(buffer = scan.nextLine(), "")) {
            System.out.println(buffer);
        }
        restoreStdOut();
        return DEFAULT_FILE_PATH;
    }

    private static void stdOutToFile() {
        LOG.info("Overloading stdOut to file with name: " + DEFAULT_FILE_PATH);
        try {
            System.setOut(new PrintStream(new FileOutputStream(DEFAULT_FILE_PATH)));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private static void restoreStdOut() {
        LOG.info("Restoring stdOut to default.");
        System.setOut(DEFAULT_STDOUT);
    }

}
