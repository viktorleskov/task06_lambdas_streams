package lambdasstreamstask.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ListGenerator {
    private static Logger LOG = LogManager.getLogger(ListGenerator.class);
    private static Random rand;

    static {
        rand = new Random();
    }

    public static List<Integer> generateRandomIntegerList(int size, int maxValue) {
        LOG.info("Generate new List of integers. Size - " + size + " MaxValue - " + maxValue);
        return Stream.generate(() -> rand.nextInt(maxValue)).limit(size).collect(Collectors.toList());
    }
}
