package lambdasstreamstask.view;

import lambdasstreamstask.controller.TaskController;
import lambdasstreamstask.interfaces.Taskeble;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Scanner;

public abstract class ConsoleView {
    private static ConsoleView singleton;
    private static Scanner scan = new Scanner(System.in);
    private static Logger LOG = LogManager.getLogger(ConsoleView.class);
    private static Map<String, String> menu;
    private static Map<String, Taskeble> methodsMenu;

    private ConsoleView() {
    }

    static {
        menu = new LinkedHashMap<>();
        menu.put("1", "1 - Input Text");
        menu.put("2", "2 - Calculate number of unique words");
        menu.put("3", "3 - Sorted list of unique words");
        menu.put("4", "4 - Word count + grouping collector");
        menu.put("5", "5 - Symbol with upper case");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", ConsoleView::inputText);
        methodsMenu.put("2", e -> ConsoleView.calculateNumberOfUniqueWords(e));
        methodsMenu.put("3", new Taskeble() {
            @Override
            public void runTask(String str) {
                ConsoleView.getSortedListOfWords(str);
            }
        });
        methodsMenu.put("4", ConsoleView::getWordCountByGrouping);
        methodsMenu.put("5", ConsoleView::getSymbolWithApperCase);
    }

    private static void show() {
        if (Optional.ofNullable(singleton).isPresent()) return;
        LOG.info("Initialization of ConsoleView");
        singleton = new ConsoleView() {
        };
        showMenu();
    }

    public static void main(String[] args) {
        show();
    }

    private static void showMenu() {
        String keyMenu;
        do {
            outputMenu();
            printToConsole("Please, select menu point.");
            keyMenu = scan.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).runTask("Hi");
            } catch (NullPointerException e) {
                System.exit(0);
            }
        } while (!keyMenu.equals("Q"));
    }

    private static void outputMenu() {
        printToConsole("\nMENU:");
        menu.values().forEach(ConsoleView::printToConsole);
    }

    private static void inputText(String str) {
        LOG.info("Asking controller for task(inputText)");
        TaskController.ask().inputText();
    }

    private static void calculateNumberOfUniqueWords(String str) {
        LOG.info("Asking controller for task(calculateNumberOfUniqueWords)");
        TaskController.ask().calculateNumberOfUniqueWords();
    }

    private static void getSortedListOfWords(String str) {
        LOG.info("Asking controller for task(getSortedListOfWords)");
        TaskController.ask().getSortedListOfWords();
    }

    private static void getWordCountByGrouping(String str) {
        LOG.info("Asking controller for task(getWordCountByGrouping)");
        TaskController.ask().getWordCountByGrouping();
    }

    private static void getSymbolWithApperCase(String str) {
        LOG.info("Asking controller for task(getSymbolWithApperCase)");
        TaskController.ask().getSymbolWithUpperCase();
    }

    public static void printToConsole(String s) {
        System.out.println(s);
    }
}
