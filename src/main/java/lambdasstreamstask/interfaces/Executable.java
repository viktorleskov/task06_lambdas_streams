package lambdasstreamstask.interfaces;

@FunctionalInterface
public interface Executable {
    void execute();
}
