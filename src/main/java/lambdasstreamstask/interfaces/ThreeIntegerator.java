package lambdasstreamstask.interfaces;

@FunctionalInterface
public interface ThreeIntegerator {
    int calculate(int a, int b, int c);
}
